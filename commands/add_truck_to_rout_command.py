from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class AddTruckToRout:
    def __init__(self, params, app_data: ApplicationData):
        validate_params_count(params, 2)
        self._params = params
        self._app_data = app_data

    def execute(self):
        rout_id = int(self._params[0])
        truck_id = int(self._params[1])
        information = self._app_data.add_truck_to_rout(
            rout_id=rout_id, truck_id=truck_id)
        if information and 1001 <= truck_id <= 1010:
            return f"Truck Scania with ID: {truck_id} was added successfully!"
        elif information and 1011 <= truck_id <= 1025:
            return f"Truck Man with ID: {truck_id} was added successfully!"
        elif information and 1026 <= truck_id <= 1040:
            return f"Truck Actros with ID: {truck_id} was added successfully!"
        else:
            return f"Could not add truck with ID:{truck_id} to route {rout_id}.\n\
The Truck is already on another Route!"
