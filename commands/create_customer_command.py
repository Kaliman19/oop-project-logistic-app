from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class CreateCustomerCommand:
    def __init__(self, params, app_data: ApplicationData):
        validate_params_count(params, 3)
        self._params = params
        self._app_data = app_data

    def execute(self):
        name = self._params[0]
        last_name = self._params[1]
        phone = self._params[2]

        customer_name, customer_last_name, customer_id, customer_phone = self._app_data.create_customer(
            first_name=name, last_name=last_name, phone_number=phone)
        return f"You have succesfully added Customer: {customer_name} {customer_last_name},:with ID: {customer_id} and Phone Number: {customer_phone}"
