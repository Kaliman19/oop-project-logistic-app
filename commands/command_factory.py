from core.application_data import ApplicationData
from commands.create_rout_command import CreateRoutCommand
from commands.create_package_command import CreatePackageCommand
from commands.add_truck_to_rout_command import AddTruckToRout
from commands.create_customer_command import CreateCustomerCommand
from commands.add_package_to_truck import AddPackageToTruck
from commands.show_routs import ShowAllRouts
from commands.find_package import FindPackageCommand
from commands.remove_package_from_truck import RemovePackageFromTruck
from commands.truck_start import TruckStart


class CommandFactory:
    def __init__(self, data: ApplicationData):
        self._app_data = data

    def create(self, input_line):
        cmd, *params = input_line.split()
        if cmd.lower() == "createroute":
            return CreateRoutCommand(params, self._app_data)
        if cmd.lower() == "createpackage":
            return CreatePackageCommand(params, self._app_data,)
        if cmd.lower() == "addtrucktoroute":
            return AddTruckToRout(params, self._app_data)
        if cmd.lower() == "createcustomer":
            return CreateCustomerCommand(params, self._app_data)
        if cmd.lower() == "addpackagetotruck":
            return AddPackageToTruck(params, self._app_data)
        if cmd.lower() == "showroutes":
            return ShowAllRouts(self._app_data)
        if cmd.lower() == "findpackage":
            return FindPackageCommand(params, self._app_data)
        if cmd.lower() == "removepackagefromtruck":
            return RemovePackageFromTruck(params, self._app_data)
        if cmd.lower() == "truckstart":
            return TruckStart(params, self._app_data)
        raise ValueError(f'Invalid command name: {cmd}!')
