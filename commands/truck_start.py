from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class TruckStart:
    def __init__(self, params, app_data: ApplicationData,):
        validate_params_count(params, 1)
        self._app_data = app_data
        self.params = params

    def execute(self):
        truck_id = int(self.params[0])
        truck, info = self._app_data.truck_start_route(truck_id)
        if info:
            return f"{truck._id}:{truck.vehicle_status} is on the road with {len(truck._uploaded_packages)} packages!"
