from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class ShowAllRouts:
    def __init__(self, app_data: ApplicationData):

        self._app_data = app_data

    def execute(self):
        info = self._app_data.show_all_routs()
        return info
