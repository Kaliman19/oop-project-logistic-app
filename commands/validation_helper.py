def validate_params_count(params, count):
    if len(params) != count:
        raise ValueError(
            f'Invalid number of arguments. Expected: {count}; received: {len(params)}.')


def try_parse_float(number):
    try:
        return float(number)
    except:
        raise ValueError(f"Invalid value, must be a number")


def try_parse_int(number):
    try:
        return int(number)
    except:
        raise ValueError(f"Invalid value, must be a number")
