from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count
from models.address import Address


class CreatePackageCommand:
    def __init__(self, params, app_data: ApplicationData):
        validate_params_count(params, 3)
        self._params = params
        self._app_data = app_data

    def execute(self):
        package_start_location = self._params[0]
        package_end_location = self._params[1]
        package_weight = float(self._params[2])

        start_address, end_address, package_id, package_weight = self._app_data.create_package(
            package_start_location, package_end_location, package_weight)

        delivery_distance = Address.get_distance(
            package_start_location, package_end_location)
        if package_weight > 50:
            delivery_cost = delivery_distance * 0.30
        else:
            delivery_cost = delivery_distance * 0.15

        return f"\nPackage with ID:{package_id} Start Location: {start_address},\
End Location: {end_address}, Weight: {package_weight} kg. was created.\nDelivery cost will be: {delivery_cost:.2f} BGN."
