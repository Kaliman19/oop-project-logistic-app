from core.application_data import ApplicationData
from commands.decoration import Decoration
from commands.validation_helper import validate_params_count


class CreateRoutCommand:
    def __init__(self, params, app_data: ApplicationData):
        self._params = params
        self._app_data = app_data

    def execute(self):
        if len(self._params) < 2:
            return "Enter at least 2 location !"

        elif len(self._params) == 2:
            start_location = self._params[0]
            end_location = self._params[1]
            current_id, current_time, expected, len_trucks, rout = self._app_data.create_rout(
                start_location, end_location)

            total_distance = rout.calculate_total_distance()
            total_fuel_cost = rout.calculate_fuel_cost()

            if len_trucks > 0:
                return f"\n{current_time}\nRout with ID:{current_id}\nStart location:'{start_location}'\
                    \nEnd location: '{end_location}' was created!\n\n" + f"\n\n Total cost of the fuel will be {total_fuel_cost}$!\n\n Total distance is {total_distance}km!"
            return f"\n{current_time}\nRout with ID:{current_id}\nStart location:'{start_location}'\
                \nEnd location: '{end_location}' was created!\n\nThere is no assign Truck to Rout! \n\nThere is no assign Truck to Rout! Please assign one!\n" + '\n\n'.join(expected) + '\n\nPlease when the truck is loaded use truckstart Command!' + f"\n\n Total cost of the fuel will be {total_fuel_cost}$!\n\n Total distance is {total_distance}km!"
        elif len(self._params) == 3:

            start_location = self._params[0]
            transit_location = self._params[1]
            end_location = self._params[2]
            current_id, current_time, expected, len_trucks, rout = self._app_data.create_rout(
                start_location, transit_location, end_location)
            total_distance = rout.calculate_total_distance()
            total_fuel_cost = rout.calculate_fuel_cost()

            if len_trucks > 0:
                return f"\n{current_time}\nRout with ID:{current_id}\nStart location:'{start_location}'\
                    \nTransit Location: '{transit_location}'\nEnd location: '{end_location}' was created!\n\\n" + '\n'.join(expected) + f"\n\n Total cost of the fuel will be {total_fuel_cost}$!\n\n Total distance is {total_distance}km!"
            return f"\n{current_time}\nRout with ID:{current_id}\nStart location:'{start_location}'\nTransit Location: '{transit_location}'\
                \nEnd location: '{end_location}' was created!\n\nThere is no assign Truck to Rout! Please assign one!\n" + '\n'.join(expected) + '\n\nPlease when the truck is loaded use truckstart Command!' f"\n\n Total cost of the fuel will be {total_fuel_cost}$!\n\n Total distance is {total_distance}km!"
