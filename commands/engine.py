from commands.command_factory import CommandFactory
from commands.decoration import Decoration
# from models.reporter import PdfReport


class Engine:
    HELPER = "You will see all available commands !"
    CITY_HELPER = ['SYD', 'MEL', 'ADL', 'ASP', 'BRI', 'DAR', 'PER']
    HELP = ["createroute - Please give at least two cities!",
            "createpackage - Please give start location, end location and weiht of the package!",
            "addtrucktoroute - Please give route ID and truck ID: 1001 - 1040!", 
            "createcustomer - Please give first name, last name and phone number! ",
            "addpackagetotruck - Please add route ID, package ID and truck ID",
            "showroutes",
            "removepackagefromtruck - Please add ID of the package and ID of the truck!",
            "truckstart - Please give truck ID"]

    output = []

    def __init__(self, factory: CommandFactory):
        self._command_factory = factory

    def start(self):
        Decoration.delay_print(
            "\n\n***Welcome to Team 3: Application!***\n\nPlease input the command that you want!\nYou can choose from the available cities:\n\n")

        for command in Engine.CITY_HELPER:
            Decoration.delay_print(f"{command} ")
        Decoration.delay_print("\n\n")
        Decoration.delay_print(Engine.HELPER)
        Decoration.delay_print("\n")
        for command in Engine.HELP:
            Decoration.delay_print(f"\n- {command}\n")
        Decoration.delay_print("\nPlease give your first command:\n")
        while True:

            input_line = input()
            if input_line.lower() == 'end':
                Decoration.delay_print_version_two("Good Bye :)!")
                report = PdfReport("Info.pdf")
                report.info = self._command_factory._app_data.all_history()
                report.generate()
                break
            try:
                command = self._command_factory.create(input_line)
                result = command.execute()

                if isinstance(result, tuple):
                    rout, package, customer = result
                    self._application_data.add_package_to_route(
                        customer, package)

                print(result)

            except ValueError as e:
                Decoration.delay_print_version_two(f"Error: {str(e)}\n")
                Decoration.delay_print_version_two("\nEnter next command.\n")
                continue

            Decoration.delay_print_version_two("\nEnter next command.\n")
