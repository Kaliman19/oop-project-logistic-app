from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class FindPackageCommand:

    def __init__(self, params, app_data: ApplicationData):
        validate_params_count(params, 1)
        self._params = params
        self._app_data = app_data

    def execute(self):
        package_id = self._params[0]

        # if self._app_data.package_exist():
        #     raise ValueError(f'Category with name {category_name} already exists!')

        package = self._app_data.find_package_by_id(package_id)

        return f' Package with id {package.id} found.'
