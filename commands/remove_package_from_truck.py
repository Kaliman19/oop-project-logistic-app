from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class RemovePackageFromTruck:
    def __init__(self, params, app_data: ApplicationData):
        validate_params_count(params, 2)
        self._params = params
        self._app_data = app_data

    def execute(self):
        id_package = int(self._params[0])
        id_truck = int(self._params[1])

        check, package = self._app_data.remove_package_from_truck(
            package_id=id_package, truck_id=id_truck)
        if check:
            return f"Package with ID:{package.id} was removed successfully!"
        return f"Package with ID:{id_package} cannot be removed! The truck is already on the road!"
