from core.application_data import ApplicationData
from commands.validation_helper import validate_params_count


class AddPackageToTruck:
    def __init__(self, params, app_data: ApplicationData):
        validate_params_count(params, 3)
        self._params = params
        self._app_data = app_data

    def execute(self):
        id_rout = int(self._params[0])
        id_package = int(self._params[1])
        id_truck = int(self._params[2])

        info = self._app_data.add_package_to_truck(
            rout_id=id_rout, package_id=id_package, truck_id=id_truck)
        return info
