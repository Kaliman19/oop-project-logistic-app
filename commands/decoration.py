import sys
import time


class Decoration:
    def delay_print(s) -> str:
        for c in s:
            sys.stdout.write(c)
            sys.stdout.flush()
            time.sleep(0.005)

    def delay_print_version_two(s) -> str:
        for c in s:
            sys.stdout.write(c)
            sys.stdout.flush()
            time.sleep(0.005)
