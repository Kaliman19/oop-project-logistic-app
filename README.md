## Name
Logistic Company Telerik OOP project


## Usage
Please go to main.py and press the "run" button. After that you will receive several commands that you may follow.


### OVERVIEW OF THE PROJECT

Design and implement a Logistics console application.
The application will be used by employees of a large Australian company aiming to expand its 
activities to the freight industry. The app will be used to manage the delivery of packages between 
hubs in major Australian cities. An employee of the company must be able to record the details of 
a delivery package, create or search for suitable delivery routes, and inspect the current state 
of delivery packages, transport vehicles and delivery routes.


The application supports the following operations:

1. Creating a delivery package – unique id, start location, end location and weight in kg, and contact information 
for the customer.

2. Creating a delivery route – should have a unique id, and a list of locations (at least two).

    a. The first location is the starting location – it has a departure time.
    
    b. The other locations have expected arrival time.

3. Search for a route based on package’s start and end locations.

4. Updating a delivery route – assign a free truck to it.

5. Updating a delivery route – assign a delivery package.

6. View a information about routes, packages and trucks.

7. The company owns the following transport vehicles:

   Vehicle ids  Name     Capacity(kg)   Max range (km)   Number of vehicles
   
   1001-1010    Scania   42000          8000             10
   
   1011-1025    Man      37000          10000            15
   
   1026-1040    Actros   26000          13000            15

8. The following distances in km were used:

       SYD  MEL  ADL   ASP   BRI  DAR   PER
   
   SYD     877   1376  2762  909 3935 4016
   
   MEL 877       725   2255 1765 3752 3509
   
   ADL 1376 725        1530 1927 3027 2785
   
   ASP 2762 2255  1530      2993 1497 2481
   
   BRI 909 1765  1927 2993       3426 4311
   
   DAR 3935 3752 3027 1497 3426       4025
   
   PER 4016 3509 2785 2481 4311  4025








