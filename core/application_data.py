from models.address import Address
from models.rout import Rout
from models.package import Package
from core.event_log import EventLog
from models.vehicles import Vehicle
from models.customer import Customer
from datetime import datetime
from models.statuses import Vehicle_Status


class ApplicationData:
    COUNTER = 1001
    HISTORY = []

    def __init__(self):
        self._rout: list[Rout] = []
        self._packages: list[Package] = []
        self._trucks: list[Vehicle] = []
        self._customers: list[Customer] = []
        self.create_vehicle()

    def create_vehicle(self):  # used to create all object Trucks at once
        for truck in range(40):
            current_truck = Vehicle(ApplicationData.COUNTER)
            self._trucks.append(current_truck)
            ApplicationData.COUNTER += 1

    def truck_start_route(self, truck_id):
        for truck in self._trucks:
            if truck._id == truck_id:
                current_truck = truck
                if len(current_truck._uploaded_packages) == 0:
                    return current_truck, False
                truck.vehicle_status = Vehicle_Status.IN_TRANSIT
                return current_truck, True

    def create_rout(self, *args):

        if len(args) > 2:
            start = args[0]
            transit = args[1]
            rout_end = args[-1]
            rout = Rout(start, transit, rout_end)
            self._rout.append(rout)
            log = EventLog(str(rout.id))
            rout.creation_time.append(log)
            expected = rout.add_days_to_now(start, transit, rout_end)
            trucks = len(rout._trucks)

            return rout.id, log.info(), expected, trucks, rout
        else:

            start = args[0]
            rout_end = args[1]
            rout = Rout(start, rout_end)
        self._rout.append(rout)
        log = EventLog(str(rout.id))
        rout.creation_time.append(log)
        expected = rout.add_days_to_now(start, rout_end)
        trucks = len(rout._trucks)

        return rout.id, log.info(), expected, trucks, rout

    def create_customer(self, first_name, last_name, phone_number):
        current_customer = Customer(
            first_name=first_name, last_name=last_name, phone_number=phone_number)
        self._customers.append(current_customer)
        return current_customer.first_name, current_customer.last_name, current_customer._id, current_customer.phone_number

    def create_package(self, start_location, end_location, weight):
        if start_location not in Address.distances:
            raise ValueError(f"Invalid start location: {start_location}")
        if end_location not in Address.distances:
            raise ValueError(f"Invalid end location: {end_location}")
        if weight <= 0:
            raise ValueError("Package weight must be greater than 0")

        current_package = Package(
            start_loc=start_location, end_loc=end_location, weight=weight)
        self._packages.append(current_package)

        return current_package.start_loc, current_package.end_loc, current_package._id, current_package.weight

    def find_package_by_id(self, id: int):
        for package in self._packages:
            if package.id == id:
                self._log_event(
                    f'Package with id {package.id} was searched for on {datetime.now().strftime("%m/%d/%Y")} at')
                return package

    def add_truck_to_rout(self, rout_id, truck_id):

        for rout in self._rout:
            if rout_id == rout.id:
                current_rout = rout
                for truck in self._trucks:
                    if truck_id == truck._id:
                        current_truck = truck
                        if current_truck.vehicle_status == Vehicle_Status.IN_TRANSIT or current_truck.vehicle_status == Vehicle_Status.LOADDING:
                            return False
                        current_rout.add_truck_to_rout(current_truck)
                        return True

    def add_package_to_truck(self, rout_id, package_id, truck_id):
        for rout in self._rout:
            if rout_id == rout._id:
                for truck in self._trucks:
                    if truck_id == truck._id:
                        current_truck = truck
                        if current_truck.vehicle_status == Vehicle_Status.IN_TRANSIT:
                            return "Cannot add this package.It is on the road !"
                        for package in self._packages:
                            if package.id == package_id:
                                current_package = package
                                current_truck.add_package_to_truck(
                                    current_package)
                                current_package.status_to_loaded()

                                return "Added Succesfuly!"
                        return "Cannot add this package !"

    def show_all_routs(self):
        output = []
        for rout in self._rout:
            output.append(
                f"Rout with ID:{rout.id}: StartLocation: {rout._start_location}, EndLocation: {rout._end_location}, Number of Trucks: {len(rout._trucks)}")
        return "\n".join(output)

    def add_package_to_rout(self, customer: Customer, package: Package):
        rout = self.check_available_routs(package)
        rout.add_package_to_truck(package)
        package.expected_delivery_time = rout.expected_time_to_arrive
        customer.add_package(package)
        self._customers.append(customer)

    def check_available_routs(self, package: Package):

        routs = []
        for rout in self._rout:
            expected_time = rout.add_days_to_now()
            routs.append((expected_time))

        routs.sort(key=lambda x: x[1])
        return routs[0][0]

    def all_history(self):
        for rout in self._rout:
            if rout._is_transit:
                ApplicationData.HISTORY.append(
                    f"Rout:{rout.id}, {rout._start_location},{rout._transit_location},{rout._end_location},{rout.expected_time_to_arrive}")
            else:
                ApplicationData.HISTORY.append(
                    f"Rout:{rout.id}, {rout._start_location},{rout._end_location},{rout.expected_time_to_arrive}")
        ApplicationData.HISTORY.append("Packages:")
        for package in self._packages:
            ApplicationData.HISTORY.append(
                f'Package:{package.id}, Status:{package.package_status},{package.weight} weight. End location: {package.end_loc}')
        return ApplicationData.HISTORY

    def remove_package_from_truck(self, package_id, truck_id):
        for truck in self._trucks:
            if truck._id == truck_id:
                current_truck = truck
                if current_truck.vehicle_status == Vehicle_Status.IN_TRANSIT:
                    return False, truck
                for package in self._packages:
                    if package._id == package_id:
                        current_package = package
                        current_truck.remove_package_from_truck(
                            current_package)
                        current_package.status_to_not_loaded()
                        return True, current_package
                    return False, current_package
