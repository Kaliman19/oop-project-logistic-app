import unittest
from models.address import Address


class TestAddress(unittest.TestCase):
    def test_valid_locations(self):
        start_location = "SYD"
        end_location = "MEL"
        address = Address(start_location, end_location)
        self.assertEqual(address.start_location, start_location)
        self.assertEqual(address.end_location, end_location)

    def test_invalid_start_location(self):
        with self.assertRaises(ValueError):
            Address("ABC", "MEL")

    def test_invalid_end_location(self):
        with self.assertRaises(ValueError):
            Address("SYD", "XYZ")

    def test_get_distance(self):
        start = "SYD"
        end = "MEL"
        self.assertEqual(Address.get_distance(start, end), 877)

    def test_check_kilometers(self):
        start_location = "SYD"
        end_location = "MEL"
        address = Address(start_location, end_location)
        kilometers = address.check_kilommeteres()
        self.assertEqual(kilometers, 877)
