import unittest
from core.application_data import ApplicationData
from commands.command_factory import CommandFactory
from commands.add_package_to_truck import AddPackageToTruck

class TestCommandFactory(unittest.TestCase):
    
    def test_create_creates_add_package_to_truck_command(self):
        app_data = ApplicationData()
        factory = CommandFactory(app_data)
        cmd = factory.create("addpackagetotruck 1 2 1001")
        self.assertIsInstance(cmd, AddPackageToTruck)
        self.assertEqual(cmd._params, ['1', '2', '1001'])
        self.assertEqual(cmd._app_data, app_data)
