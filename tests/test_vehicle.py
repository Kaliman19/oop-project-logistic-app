import unittest
from models.package import Package
from models.statuses import Package_Status


class TestVehicle(unittest.TestCase):
    def setUp(self):
        self.package = Package("SYD", "MEL", 40)

    def test_package_start_loc_valid(self):
        self.assertEqual(self.package.start_loc, "SYD")

    def test_package_end_loc_valid(self):
        self.assertEqual(self.package.end_loc, "MEL")

    def test_package_weight_valid(self):
        self.assertEqual(self.package.weight, 40)

    def test_package_expected_delivery_time_valid(self):
        self.assertEqual(self.package.expected_delivery_time, "")

    def test_package_status_valid(self):
        self.assertEqual(self.package.package_status,
                         Package_Status.NOT_LOADED)

    def test_status_to_loaded_return_valid_status(self):
        self.package.status_to_loaded()
        self.assertEqual(self.package.package_status, Package_Status.LOADED)

    def test_status_to_not_loaded_return_valid_status(self):
        self.package.status_to_not_loaded()
        self.assertEqual(self.package.package_status,
                         Package_Status.NOT_LOADED)

    def test_id_is_readonly(self):
        with self.assertRaises(AttributeError):
            self.package.id = 2

    def test_set_expected_delivery_time(self):
        self.package.expected_delivery_time = "2023-02-28"
        self.assertEqual(self.package.expected_delivery_time, "2023-02-28")
