import unittest
from models.package import Package


class TestPackage(unittest.TestCase):
    def setUp(self):
        self.package = Package("SYD", "MEL", 40)

    def test_package_start_loc_valid(self):
        self.assertEqual(self.package.start_loc, "SYD")

    def test_package_end_loc_valid(self):
        self.assertEqual(self.package.end_loc, "MEL")

    def test_status_to_loaded_return_valid_status(self):
        self.package.status_to_loaded()
        self.assertEqual(self.package.package_status, "Loaded")
