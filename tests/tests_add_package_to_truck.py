import unittest
from unittest.mock import Mock
from commands.add_package_to_truck import AddPackageToTruck
from core.application_data import ApplicationData


class TestAddPackageToTruck(unittest.TestCase):
    def test_execute(self):
        params = ['1', '2', '3']
        app_data = Mock(spec=ApplicationData)
        app_data.add_package_to_truck.return_value = True

        command = AddPackageToTruck(params, app_data)
        result = command.execute()

        app_data.add_package_to_truck.assert_called_once_with(
            rout_id=1, package_id=2, truck_id=3)
        self.assertTrue(result)

    def test_execute_with_invalid_params_count(self):
        params = ['1', '2']
        app_data = Mock(spec=ApplicationData)

        with self.assertRaises(ValueError):
            AddPackageToTruck(params, app_data)
