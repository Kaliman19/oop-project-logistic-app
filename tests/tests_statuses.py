import unittest
from models.statuses import Vehicle_Status, Package_Status


class TestVehicleStatus(unittest.TestCase):
    def test_from_string(self):
        self.assertEqual(Vehicle_Status.from_string('Waiting'), 'Waiting')
        self.assertEqual(Vehicle_Status.from_string(
            'In transit'), 'In transit')

    def test_from_string_raises_exception_with_invalid_status_string(self):
        with self.assertRaises(ValueError):
            Vehicle_Status.from_string('invalid status')


class TestPackageStatus(unittest.TestCase):
    def test_from_string(self):
        self.assertEqual(Package_Status.from_string(
            'Not loaded'), 'Not loaded')
        self.assertEqual(Package_Status.from_string('Loaded'), 'Loaded')
        self.assertEqual(Package_Status.from_string(
            'In transit'), 'In transit')
        self.assertEqual(Package_Status.from_string('Delivered'), 'Delivered')

    def test_from_string_raises_exception_with_invalid_status_string(self):
        with self.assertRaises(ValueError):
            Package_Status.from_string('invalid status')
