import unittest
from models.rout import Rout
from models.vehicles import Vehicle
from models.statuses import Vehicle_Status


class TestAddTrucktoRoute(unittest.TestCase):
    VALID_START_LOC = "SYD"
    VALID_END_LOC = "MEL"
    VALID_TRUCK_ID = 1001
    AVERAGE_CONSUMPTION = 20
    FUEL_PRICE = 10
    VALID_VEHICLE_STATUS = Vehicle_Status.IN_TRANSIT

    def test_ifTruck_canNOT_Be_AddedtoRoute(self):
        truck = Vehicle(1001, 20, 10)
        route = Rout(self.VALID_START_LOC, self.VALID_END_LOC)
        truck.vehicle_status = Vehicle_Status.IN_TRANSIT
        self.assertEqual(route.add_truck_to_rout(truck),
                         "This truck is assigned to Rout already !")

    def test_ifTruck_CAN_Be_AddedtoRoute(self):
        truck = Vehicle(1001, 20, 10)
        route = Rout(self.VALID_START_LOC, self.VALID_END_LOC)
        truck.vehicle_status = Vehicle_Status.WAITING
        route.add_truck_to_rout(truck)
        self.assertEqual(truck.vehicle_status, Vehicle_Status.LOADDING)
        self.assertEqual(len(route._trucks), 1)
