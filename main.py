from models.rout import Rout
from models.package import Package
from core.event_log import EventLog
from models.vehicles import Vehicle
from core.application_data import ApplicationData
from datetime import date, timedelta, datetime
from commands.command_factory import CommandFactory
from commands.decoration import Decoration
from commands.engine import Engine
# from models.reporter import PdfReport
app_data = ApplicationData()
cmd_factory = CommandFactory(app_data)
engine = Engine(cmd_factory)
engine.start()

