# import webbrowser
# from fpdf import FPDF
# from datetime import date, timedelta, datetime
#
#
# class PdfReport:
#
#     def __init__(self, filename):
#         self.filename = filename
#         self.info = []
#
#     def generate(self):
#         """"
#         Generating PDF file
#         """
#
#         pdf = FPDF(orientation="P", unit="pt", format="A3")
#         pdf.add_page()
#
#         pdf.set_font(family="Times", size=24, style="B")
#         pdf.cell(w=0, h=80, txt="Applycation Data", border=1, ln=1, align="C")
#         currentDateAndTime = datetime.now()
#         date_time = currentDateAndTime.strftime("%m/%d/%Y, %H:%M")
#
#         pdf.set_font(family="Times", size=14, style="B")
#         pdf.cell(w=120, h=80, txt="Period:", border=1, ln=0)
#         pdf.cell(w=0, h=80, txt=str(date_time), border=1, ln=1)
#         for i in range(len(self.info)):
#             current_text = "".join(self.info[i])
#             pdf.cell(w=0, h=80, txt=str(self.info[i]), border=1, ln=1)
#
#         pdf.output(self.filename)
#
#         webbrowser.open(self.filename)
