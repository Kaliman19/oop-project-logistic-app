from models.package import Package
from models.statuses import Vehicle_Status,Package_Status


class Vehicle:
    ID_TO_NAME = {
        1001: "Scania",
        1002: "Scania",
        1003: "Scania",
        1004: "Scania",
        1005: "Scania",
        1006: "Scania",
        1007: "Scania",
        1008: "Scania",
        1009: "Scania",
        1010: "Scania",
        1011: "Man",
        1012: "Man",
        1013: "Man",
        1014: "Man",
        1015: "Man",
        1016: "Man",
        1017: "Man",
        1018: "Man",
        1019: "Man",
        1020: "Man",
        1021: "Man",
        1022: "Man",
        1023: "Man",
        1024: "Man",
        1025: "Man",
        1026: "Actros",
        1027: "Actros",
        1028: "Actros",
        1029: "Actros",
        1030: "Actros",
        1031: "Actros",
        1032: "Actros",
        1033: "Actros",
        1034: "Actros",
        1035: "Actros",
        1036: "Actros",
        1037: "Actros",
        1038: "Actros",
        1039: "Actros",
        1040: "Actros",
    }
    AVERAGE_CONSUMPTION = 30
    FUEL_PRICE = 3
    
    SCANIA_CAPACITY = 42000
    MAN_CAPACITY = 37000
    ACTROS_CAPACITY = 26000

    SCANIA_MAX_RANGE = 8000
    MAN_MAX_RANGE = 10000
    ACTROS_MAX_RANGE = 13000

    SCANIA_NUM = 10
    MAN_NUM = 15
    ACTROS_NUM = 15

    def __init__(self, id: int, average_consumption=AVERAGE_CONSUMPTION, fuel_price=FUEL_PRICE):
        self._average_consumption = average_consumption
        self._fuel_price = fuel_price
        self._id = id
        self._uploaded_packages: list[Package] = []
        self._name = Vehicle.ID_TO_NAME.get(id, "Unknown")
        if self._name == "Scania":
            self._capacity = Vehicle.SCANIA_CAPACITY
            self._max_range_km = Vehicle.SCANIA_MAX_RANGE
            Vehicle.SCANIA_NUM -= 1
        elif self._name == "Man":
            self._capacity = Vehicle.MAN_CAPACITY
            self._max_range_km = Vehicle.MAN_MAX_RANGE
            Vehicle.MAN_NUM -= 1
        elif self._name == "Actros":
            self._capacity = Vehicle.ACTROS_CAPACITY
            self._max_range_km = Vehicle.ACTROS_MAX_RANGE
            Vehicle.ACTROS_NUM -= 1
        else:
            raise ValueError("Unknown truck ID")

        self._count = [Vehicle.SCANIA_NUM, Vehicle.MAN_NUM, Vehicle.ACTROS_NUM]
        self.vehicle_status = Vehicle_Status.WAITING


    @property
    def uploaded_packages(self):
        return tuple(self._uploaded_packages)

    @property
    def capacity(self):
        return self._capacity

    @property
    def max_range_km(self):
        return self._max_range_km

    def add_package_to_truck(self, package: Package):
        if package.package_status==Package_Status.LOADED:
            raise ValueError('Package already loaded')
        if self._capacity > int(package.weight):
            self._uploaded_packages.append(package)
            self._capacity -= int(package.weight)
            package.package_status=Package_Status.LOADED
        else:
            raise ValueError("We cannot add this package! This truck is full!")

    def remove_package_from_truck(self, package: Package):
        if len(self._uploaded_packages) > 0:
            for item in self._uploaded_packages:
                if item._id == package._id:
                    self._uploaded_packages.remove(item)
                    self._capacity += int(package.weight)
                    package.package_status=Package_Status.DELIVERED
                    # this is used to remove the package from rout also.
                    return True

            return False  # if this is False, the package cannot be removed so we will have error

    def __str__(self):
        output = [

            f'name: {self._name}',
            f'capacity: {self._capacity}',
            f'packages: {len(self._uploaded_packages)}',
            f'max range in km: {self.max_range_km}',
            f'number of vehicles: {self._count}',
        ]

        return '\n'.join(output)
    
