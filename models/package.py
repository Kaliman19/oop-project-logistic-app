from models.statuses import Package_Status
from models.address import Address


class Package:
    ID = 1

    def __init__(self, start_loc, end_loc, weight):
        self._id = Package.ID
        Package.ID += 1
        self.start_loc = start_loc
        self.end_loc = end_loc
        self.weight = weight
        self.expected_delivery_time = ""
        self.package_status = Package_Status.NOT_LOADED

    @property
    def id(self):
        return self._id

    def status_to_loaded(self):
        self.package_status = Package_Status.LOADED

    def status_to_not_loaded(self):
        self.package_status = Package_Status.NOT_LOADED
