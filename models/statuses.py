class Vehicle_Status:
    WAITING = 'Waiting'
    IN_TRANSIT = 'In transit'
    LOADDING = "Loading"

    @classmethod
    def from_string(cls, status_string):
        if status_string not in [cls.WAITING, cls.IN_TRANSIT]:
            raise ValueError(
                f'None of the possible Status values matches the value {status_string}.')

        return status_string


class Package_Status:
    NOT_LOADED = 'Not loaded'
    LOADED = 'Loaded'
    IN_TRANSIT = 'In transit'
    DELIVERED = 'Delivered'

    @classmethod
    def from_string(cls, package_string):
        if package_string not in [cls.LOADED, cls.IN_TRANSIT, cls.DELIVERED, cls.NOT_LOADED]:
            raise ValueError(
                f'None of the possible Status values matches the value {package_string}.')

        return package_string
