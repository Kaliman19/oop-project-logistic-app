class Address:
    distances = {
        'SYD': {'MEL': 877, 'ADL': 1376, 'ASP': 2762, 'BRI': 909, 'DAR': 3935, 'PER': 4016},
        'MEL': {'SYD': 877, 'ADL': 725, 'ASP': 2255, 'BRI': 1765, 'DAR': 3752, 'PER': 3509},
        'ADL': {'SYD': 1376, 'MEL': 725, 'ASP': 1530, 'BRI': 1927, 'DAR': 3027, 'PER': 2785},
        'ASP': {'SYD': 2762, 'MEL': 2255, 'ADL': 1530, 'BRI': 2993, 'DAR': 1497, 'PER': 2481},
        'BRI': {'SYD': 909, 'MEL': 1765, 'ADL': 1927, 'ASP': 2993, 'DAR': 3426, 'PER': 4311},
        'DAR': {'SYD': 3935, 'MEL': 3752, 'ADL': 3027, 'ASP': 1497, 'BRI': 3426, 'PER': 4025},
        'PER': {'SYD': 4016, 'MEL': 3509, 'ADL': 2785, 'ASP': 2481, 'BRI': 4311, 'DAR': 4025},
    }

    def __init__(self, start_location, end_location):
        if start_location not in self.distances:
            raise ValueError(f"Invalid start location: {start_location}")
        if end_location not in self.distances:
            raise ValueError(f"Invalid end location: {end_location}")
        self.start_location = start_location
        self.end_location = end_location

    @staticmethod
    def get_distance(start, end):
        return Address.distances[start][end]
    
    def check_kilommeteres(self):
        return Address.get_distance(self._start_location, self._end_location)
    
    def check_kilommeteres(self):
        return self.distances[self.start_location][self.end_location]
    


    