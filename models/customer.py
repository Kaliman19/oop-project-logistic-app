from models.package import Package


class Customer:
    ID = 1

    def __init__(self, first_name: str, last_name: str, phone_number: str):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        self.packages = []
        self._id = Customer.ID
        Customer.ID += 1

    def add_package(self, package: Package):
        self.packages.append(package)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
