from datetime import date, timedelta, datetime
from models.vehicles import Vehicle
from models.package import Package
from core.event_log import EventLog
from models.address import Address
from models.statuses import Vehicle_Status, Package_Status
import math


class Rout():
    ID = 1

    time = datetime.now() + timedelta(hours=6)
    current_time = time.strftime('%d %m %Y')

    def __init__(self, *args):
        self._is_transit = False
        self._id = Rout.ID
        Rout.ID += 1
        self._start_location = args[0]
        self._end_location = args[-1]
        if len(args) > 2:
            self._transit_location = args[1]
            self._is_transit = True

        self._expected_time_to_arrive = ""
        self._trucks: list[Vehicle] = []  # for every truck will add package
        self._packages: list[Package] = []
        self.creation_time = []

    @property
    def id(self):
        return self._id

    def add_days_to_now(self, *args):
        if len(args) < 2:
            raise ValueError("You need a second city")
        total_distance = 0
        start_time = Rout.time
        i = 1
        total_info = []
        while i < len(args):

            current_address = Address(args[i - 1], args[i])
            total_distance = current_address.check_kilommeteres()
            expected_days = math.floor((total_distance / 87) / 24)
            expected_hours = ((total_distance / 87) % 24)
            d = start_time + timedelta(days=expected_days) + \
                timedelta(hours=expected_hours)
            end_time = d.strftime("%D %H:%M")
            total_info.append(f'Expected to arrive at {args[i]} on {end_time}')
            start_time = d
            i += 1
        end_loc_time = d.strftime('%d %m %Y')
        self._expected_time_to_arrive += end_loc_time
        return total_info

    def get_total_distance(self):
        if self._is_transit:
            distance1 = Address.get_distance(
                self._start_location, self._transit_location)
            distance2 = Address.get_distance(
                self._transit_location, self._end_location)
            total_distance = distance1 + distance2
        else:
            total_distance = Address.get_distance(
                self._start_location, self._end_location)
        return total_distance

    def calculate_fuel_cost(self):
        total_distance = self.get_total_distance()
        total_fuel_cost = (
            total_distance * Vehicle.AVERAGE_CONSUMPTION * Vehicle.FUEL_PRICE) / 100
        return total_fuel_cost

    def add_truck_to_rout(self, truck: Vehicle):
        if truck.vehicle_status == Vehicle_Status.IN_TRANSIT:
            return "This truck is assigned to Route already !"
        else:
            self._trucks.append(truck)
            truck.vehicle_status = Vehicle_Status.LOADDING

    def add_package_to_truck(self, package: Package):
        if package.package_status == Package_Status.LOADED:
            raise ValueError('Package already loaded')
        if len(self._trucks) > 0:
            for truck in self._trucks:
                truck.add_package_to_truck(package=package)
                self._packages.append(package)
                package.package_status = Package_Status.LOADED
                return f"Package with ID:{package.ID} was added to truck {truck._name} with ID:{truck._id}"
        return "No trucks in current Rout yet!"

    def rout_trucks_info(self):
        trucks_capacity = []
        for truck in self._trucks:
            trucks_capacity.append(
                f"Capacity of the truck with ID:{truck._id} is {truck._capacity}kg.\nTotal number of packages in the truck is:{len(self._packages)}")
        return "\n".join(trucks_capacity)

    @property
    def expected_time_to_arrive(self):
        return self._expected_time_to_arrive

    @expected_time_to_arrive.setter
    def expected_time_to_arrive(self, value):
        self._expected_time_to_arrive = value

    def calculate_total_distance(self):
        distance = 0
        if self._is_transit:
            distance = self.calculate_distance(self._start_location, self._transit_location) + \
                self.calculate_distance(
                    self._transit_location, self._end_location)
        else:
            distance = self.calculate_distance(
                self._start_location, self._end_location)
        return distance

    @staticmethod
    def calculate_distance(start, end, transit_location=None):
        if transit_location:
            return Address.distances[start][transit_location] + Address.distances[transit_location][end]
        return Address.distances[start][end]
